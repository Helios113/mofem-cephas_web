### MoFEM v0.11.0

- Added functionality for matrix functions and their derivatives
- Fixes of form integrators
- Added new tensor operators
- Novel functionality of marking DOFs for boundary conditions
- Extended handling of node data from `med` and `rmed` files
- Implementation of quad elements
- Extension of Docker functionality
- Installation with new version of `spack`
- Support for `Jupyter` notebooks with `Docker`
- Various minor fixes and improvements

----

### MoFEM v0.10.0

- Improved memory and run-time efficiency
- Fixes and improvements in the mesh-cutting algorithm
- New method of enforcing boundary conditions
- Refactoring of the implementation of multi-indices
- Refactoring of logging
- Changes on web-pages
- New tutorials and changes for tutorials web-pages 
- Various minor bug fixes

----

### MoFEM v0.9.2

- New logging interface based on boost.log
- Fixes in PrismInterface
- Developments of mesh cutting algorithm
- Interface for side volume elements for contact prism elements
- H-div for contact elements
- Surface pressure ALE
- Contact element ALE
- Gauss point convection for contact (moderate slip)
- ALM frictionless contact
- Computation of the real contact area and active set of gauss points
- Rotational Dirichlet BCs
- 8 new lessons
- New tutorials
- Code refactoring and minor fixes